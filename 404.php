<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package Koutabase
 */

get_header();
?>

<?php get_template_part( 'template-parts/banner' ); // Banner. ?>

<div id="inner-content" class="container">

	<div id="main" class="main clearfix" role="main">

		<section class="error-404 not-found text-center clearfix">

			<header class="page-header">

				<h1 class="page-title"><?php esc_html_e( 'Hups! Sivua ei löytynyt.', 'koutabase' ); ?></h1>

			</header>

			<div class="page-content">

				<p><?php esc_html_e( 'Näyttää siltä, että tästä sijainnista ei löytynyt mitään. Tämä voi johtua siitä, että sivu on siirretty, nimetty uudestaan tai poistettu.', 'koutabase' ); ?></p>

			</div>

		</section>

	</div><?php // END #main ?>

</div><?php // END #inner-content ?>

<?php
get_footer();
