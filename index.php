<?php
/**
 * Default Template
 *
 * Used if no other suitable template is available.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();

global $kb_layout_class;
?>

<?php get_template_part( 'template-parts/banner' ); // Banner ?>

<div id="inner-content">

	<div id="main" class="clearfix" role="main">

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single-article' ); // Single article content. ?>

		<?php endwhile; ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

	<?php endif; ?>

	</div><?php // END #main ?>

</div><?php // END #inner-content ?>

<?php
get_footer();
