<?php
/**
 * Koutabase Main Functions
 *
 * Add custom functions and edit thumbnail sizes, header images, sidebars, menus, etc.
 *
 * @link https://bitbucket.org/koutamiika/koutabase/src/master/
 * @link https://codex.wordpress.org/Theme_Development
 *
 * @package Koutabase
 *
 * @todo language support, customizer functions
 *
 * Table of Contents
 *
 * 1.0 - Include Files
 * 2.0 - Scripts & Styles
 * 3.0 - Theme Support
 * 4.0 - Menus & Navigation
 * 5.0 - Images & Headers
 * 6.0 - Sidebars
 * 7.0 - Search Functions
 * 8.0 - Comment Layout
 * 9.0 - Utility Functions
 * 10.0 - WooCommerce
 * 11.0 - Custom/Additional Functions
 */

define( 'KOUTABASE_THEME_VERSION', '1.0' );
define( 'KOUTABASE_INCLUDE_PATH', dirname( __FILE__ ) . '/includes/' );


/************************************
 * 1.0 - INCLUDE FILES
 */

// Add any additional files to include here.
require_once KOUTABASE_INCLUDE_PATH . 'base-functions.php';
require_once KOUTABASE_INCLUDE_PATH . 'tinymce-settings.php';

/************************************
 * 2.0 - SCRIPTS & STYLES
 */

require_once KOUTABASE_INCLUDE_PATH . 'enqueues.php';

/************************************
 * 3.0 - THEME SUPPORT
 */

require_once KOUTABASE_INCLUDE_PATH . 'theme-support.php';

/************************************
 * 4.0 - MENUS & NAVIGATION
 */

// Walker for koutabase_main_nav().
require_once KOUTABASE_INCLUDE_PATH . '/classes/class-koutabase-walker-nav-menu.php';
require_once KOUTABASE_INCLUDE_PATH . 'menus.php';

/************************************
 * 5.0 - IMAGES & HEADERS
 */

require_once KOUTABASE_INCLUDE_PATH . 'images.php';
require_once KOUTABASE_INCLUDE_PATH . '/classes/class-koutabase-svg-icons.php';
require_once KOUTABASE_INCLUDE_PATH . '/svg-icons.php';

/************************************
 * 6.0 - SIDEBARS
 */

require_once KOUTABASE_INCLUDE_PATH . 'sidebars.php';

/************************************
 * 7.0 - SEARCH FUNCTIONS
 */

require_once KOUTABASE_INCLUDE_PATH . 'search-functions.php';

/************************************
 * 8.0 - COMMENT RELATED FUNCTIONS
 */

require_once KOUTABASE_INCLUDE_PATH . 'comments-functions.php';

/************************************
 * 9.0 - UTILITY FUNCTIONS
 */

require_once KOUTABASE_INCLUDE_PATH . 'utilities.php';

/************************************
 * 10.0 - WOOCOMMERCE
 */

if ( class_exists( 'Woocommerce' ) ) {
	require_once KOUTABASE_INCLUDE_PATH . '/classes/class-koutabase-woocommerce.php';
}

/************************************
 * 11.0 CUSTOM/ADDITIONAL FUNCTIONS
 */

require_once KOUTABASE_INCLUDE_PATH . 'gutenberg.php';
require_once KOUTABASE_INCLUDE_PATH . 'widgets.php';
