<?php
/**
 * The template for displaying search forms
 *
 * @package Koutabase
 */

?>
<form method="get" id="searchform" class="clearfix" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<label class="screen-reader-text" for="s"><?php esc_html_e( 'Hae sivustolta:', 'koutabase' ); ?></label>
	<input type="text" name="s" id="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php esc_attr_e( 'Hae sivustolta&hellip;', 'koutabase' ); ?>" />
	<input type="submit" id="searchsubmit" value="<?php esc_attr_e( 'Hae', 'koutabase' ); ?>" />
</form>
