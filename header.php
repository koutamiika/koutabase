<?php
/**
 * Header Template
 *
 * @see http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js<?php echo ( is_user_logged_in() ) ? ' has-admin-bar' : ''; ?>">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="theme-color" content="#111111">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php koutabase_schema_markup( 'body', true ); ?>>

	<?php wp_body_open(); ?>

	<div id="container">

		<?php get_template_part( 'template-parts/content', 'header' ); ?>

		<div id="content">

			<?php do_action( 'koutabase_after_content_begin' ); ?>
