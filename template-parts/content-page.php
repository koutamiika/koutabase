<?php
/**
 * Page content
 *
 * @package Koutabase
 */

?>

<article id="page-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

	<header class="page-header">

		<?php if ( apply_filters( 'koutabase_show_page_title', true ) ) : ?>

			<h1 class="page-title text-center"><?php the_title(); ?></h1>

		<?php endif; ?>

	</header>

	<section class="entry-content clearfix">

		<?php the_content(); ?>

		<?php
		wp_link_pages(
			array(
				'before'      => '<div class="page-links">',
				'after'       => '</div>',
				'link_before' => '',
				'link_after'  => '',
			)
		);
		?>

	</section>

</article>
