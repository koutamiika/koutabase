<?php
/**
 * Banner
 *
 * @package Koutabase
 */

// extra classes
$class = array();
// title
if ( is_home() ) {
	$page_title = get_the_title( get_option( 'page_for_posts' ) ) ?: 'Blog';
} elseif ( is_singular() ) {
	$page_title = get_the_title();
} else {
	$page_title = koutabase_get_the_archive_title();
}

// description
$description = '';
if ( is_singular() ) {
	$description = get_post_meta( get_the_ID(), 'lead', true );
} elseif ( is_tag() || is_category() || is_tax() ) {
	$description = get_the_archive_description();
}

// background
$image = '';
if ( is_singular() && has_post_thumbnail() ) {
	$image = get_the_post_thumbnail( get_the_ID(), 'full', array( 'loading' => false ) );
} elseif ( is_home() ) {
	$image = get_the_post_thumbnail( get_option( 'page_for_posts' ), 'full', array( 'loading' => false ) );
}

if ( ! empty( $image ) ) {
	$class[] = 'hero--has-background';
} else {
	$class[] = 'hero--no-background';
}
?>

<div class="banner-wrap <?php echo implode( ' ', $class ); ?>" <?php koutabase_schema_markup( 'banner', true ); ?>>

	<?php if ( ! empty( $image ) ) : ?>

		<div id="banner">

			<?php echo wp_kses_post( $image ); ?>

		</div>

	<?php elseif ( get_custom_header_markup() ) : ?>

		<div id="banner">
			<?php the_custom_header_markup(); ?>
		</div>

	<?php endif; ?>

	<?php if ( ! is_single() ) : ?>

	<div class="banner__container">

		<h1 class="banner__title"><?php echo wp_kses_post( $page_title ); ?></h1>

		<?php if ( ! empty( $description ) ) : ?>

			<p class="banner__description"><?php echo wp_kses_post( $description ); ?></p>

		<?php endif; ?>

	</div>

	<?php endif; ?>

</div>
