<?php
/**
 * Error Messages
 *
 * @package Koutabase
 */

?>
<section class="post-not-found text-center clearfix">

	<header class="page-header text-center">

		<h1><?php esc_html_e( 'Mitään ei löytynyt', 'koutabase' ); ?></h1>

	</header>

	<div class="page-content">

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<?php // translators: add new post url. ?>
			<p><?php printf( __( 'Valmiina julkaisemaan ensimmäisen artikkelisi? <a href="%1$s">Aloita tästä</a>.', 'koutabase' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Valitettavasti hakutermeille ei löytynyt tuloksia. Yritä uudestaan.', 'koutabase' ); ?></p>

			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php esc_html_e( 'Emme löytäneet sitä mitä yritit etsiä. Ehkäpä hakutoiminnon käyttäminen auttaa?', 'koutabase' ); ?></p>

			<?php get_search_form(); ?>

		<?php endif; ?>

	</div>

</section>
