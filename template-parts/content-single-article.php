<?php
/**
 * Template for single article content.
 *
 * @package Koutabase
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" <?php koutabase_schema_markup( 'blogpost', true ); ?>>

	<header class="entry-header">

		<h1 class="entry-title h2" <?php koutabase_schema_markup( 'title', true ); ?>><?php the_title(); ?></h1>

	</header>

	<section class="entry-content" <?php koutabase_schema_markup( 'article_body', true ); ?>>

		<?php the_content(); ?>

		<?php
		wp_link_pages(
			array(
				'before'      => '<div class="page-links">',
				'after'       => '</div>',
				'link_before' => '',
				'link_after'  => '',
			)
		);
		?>

	</section>

	<footer class="entry-footer">

		<?php koutabase_post_meta(); ?>

	</footer>

</article>
