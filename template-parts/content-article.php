<?php
/**
 * Article template. Used on archive and blog pages.
 *
 * @package Koutabase
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" <?php koutabase_schema_markup( 'blogpost', true ); ?>>

	<?php if ( has_post_thumbnail() ) : ?>

		<div class="post__thumbnail">

			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail( 'koutabase-thumb-550x310' ); ?>
			</a>

		</div>

	<?php endif; ?>

	<header class="entry-header">

		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark">
			<?php if ( is_sticky() ) : ?>
				<?php koutabase_the_theme_svg( 'sticky' ); ?>
			<?php endif; ?>
			<?php the_title(); ?>
			</a>
		</h1>

	</header>

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div>

	<footer class="entry-footer">
		<?php koutabase_post_meta(); ?>
	</footer>

</article>
