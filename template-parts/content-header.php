<?php
/**
 * Displays main navigation and logo.
 *
 * @package Koutabase
 */

?>

<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Siirry suoraan sisältöön', 'koutabase' ); ?></a>

<header id="masthead" class="header" <?php koutabase_schema_markup( 'header', true ); ?>>

	<?php do_action( 'koutabase_after_header_begin' ); ?>

	<div id="inner-header" class="container">

		<div class="row align-items-center justify-content-between">

			<?php // to use an image just replace the bloginfo('name') with <img> ?>
			<div id="logo" class="col-auto h1" <?php koutabase_schema_markup( 'logo', true ); ?>>
				<?php // Show logo if it is set. Else show blogname. ?>
				<?php if ( has_custom_logo() ) : ?>
					<?php the_custom_logo(); ?>
				<?php else : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
						<?php bloginfo( 'name' ); ?>
					</a>
				<?php endif; ?>
			</div>

			<nav id="main-navigation" class="clearfix" aria-label="<?php esc_attr_e( 'Ensisijainen valikko', 'koutabase' ); ?>" <?php koutabase_schema_markup( 'nav', true ); ?>>
				<?php koutabase_main_nav(); ?>
			</nav>

			<div id="mobile-menu-toggle" class="col-auto">
				<button id="mobile-menu-button" type="button"><?php esc_html_e( 'Valikko', 'koutabase' ); ?></button>
			</div>

		</div>

	</div>

	<?php do_action( 'koutabase_before_header_end' ); ?>

</header>

<nav id="main-navigation-mobile" class="clearfix" aria-label="<?php esc_attr_e( 'Mobiilivalikko', 'koutabase' ); ?>" <?php koutabase_schema_markup( 'nav', true ); ?>>
	<?php koutabase_mobile_nav(); ?>
</nav>
