<?php
/**
 * Pagination
 *
 * @package Koutabase
 */

if ( function_exists( 'koutabase_page_navi' ) ) :
	global $wp_query;
	koutabase_page_navi( '', '', $wp_query );
else :
	?>

	<nav class="wp-prev-next">
		<ul class="clearfix">
			<li class="prev-link">
				<?php next_posts_link( __( '&laquo; Vanhemmat', 'koutabase' ) ); ?>
			</li>
			<li class="next-link">
				<?php previous_posts_link( __( 'Uudemmat &raquo;', 'koutabase' ) ); ?>
			</li>
		</ul>
	</nav>

	<?php
endif;
