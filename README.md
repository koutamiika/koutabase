KoutaBase Theme
=======

KoutaBase theme is started theme for most Kouta developed WordPress themes. KoutaBase is based on Scaffolding by Hall.

Developed by Kouta Media Oy
https://www.koutamedia.fi/

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Submit Bugs & or Fixes:
https://bitbucket.org/koutamiika/koutabase/issues

## 1. Directory structure

`/css/` includes all minified css files and fonts.

`/scss/` has all sass files.

`/js/` has all custom js files.

`/libs/` thirdparty libraries such as modernizr, magnific popup etc.

`/images/` all images and graphics used by the theme.

`/includes/` has all php files that are not part of template structure.

`/template-part/` has small template parts meant to be used with function `get_template_part`.

`/templates/` all the page templates that user can select when editing pages.

## 2. Setup

### 2.1 Install

Do these theme installation steps before modifying anything.

1. (optional) Start a project by using our [BAT script](https://bitbucket.org/koutamiika/kouta-deploy-wp/src/master/) which installs WordPress and downloads the latest version of Koutabase theme.
2. Download this repository
3. Extract into /wp-content/themes/ and rename the project folder as `sitename`
4. Activate the theme

### 2.2 Install build tools

These must be done before developing the theme.

1. Open theme folder in terminal
2. Run `npm install` to install all build tools. or install them individually.
	1. Run `npm install gulp@3.9.1` or `npm install gulp` (there has been some issues with later versions of gulp, thus version 3.9.1)
	2. Run `npm install gulp-compass --save-dev`
	3. Run `npm install browser-sync --save`
	4. Run `npm install wp-cli --save`

### 2.3 Start working

These must be done before developing the theme.

1. Open theme folder in terminal
2. Run `gulp` to activate build process in background. You'll get development proxy at http://localhost:3000 where changes to code will be updated automatically to browser (no need to reload).
3. To quit press `ctrl` + `c`

## 3. Styles

Styles are written in SASS in `/scss/`. There are 3 main stylesheets that are compiled to `/css/`.

  * `style.scss` front-end styles of website
  * `editor-styles.scss` back-end styles for Classic Editor and Block editor
  * `login.scss` front-end styles for login screen

### 3.1 SASS structure

  * `/elements/` universal styles for elements
  * `/libs/` thirdparty library styles
  * `/media-queries/` breakpoints
  * `/modules/`
  * `/plugins/` Plugin related styles
      * `/woocommerce/` WooCommerce related styles. These are loaded only if WooCommerce is activated.
  * `/utilities/` variables, mixins and other utilities

### 3.2 Workflow

  * Start working the project by running `gulp`
  * You get Browsersync URL at `https://localhost:3000` (check yours in console). Here the styles will automatically reload when you save your changes.
  * If you make a syntax error, a console will send you a notification and tell where the error is. If Gulp stops running, start it again with `gulp`.
  * In browser developer tools shows what styles is located in which SASS partial file (SASS Maps)

### 3.3 Adding new files

  1. Make a new file like `/scss/elements/_card.scss`
  2. Import the new file in `style.scss` with `@import "elements/card";`

### 3.4 Tips

 * Don't hesitate to create variables if you have repeating values. Put all variable definitions in `/utilities/_variables.scss`.

## 4. Scripts

By default these scripts are included.

  * SelectWoo
  * Magnific Popup
  * Retina.js
  * Modernizr
  * Unveil

### 4.1 Directory structure

  * `/libs/js/` thirdparty components
  * `scripts.js` main js file that is run in footer

## 5. SVG and Images

Put your static images in `/images/`.

### 5.1 Image sizes

Image sizes are defined in `/includes/images.php`. Tips for creating image sizes:

  * Base images on commonly used aspect ratios (16:9, 1:1)
  * Make "medium" half of the text columns and "large" full text column

### 5.2 Lazy load

Theme uses Unveil.js for lazy loading. Support Lazy loading for now is pretty limited and is under development.

### 5.3. Favicons

Favicon should be added via WordPress customizer.

## 6. Includes

Includes is a place for all PHP files that are not part of templates. So all classes, filters, actions and functions etc.

All the functions, hooks and setup should be on their own filer organized at /includes/. The names of files should describe what the file does.

### 6.1 Functions.php

The `functions.php` is the place to require all PHP files that are not part of the current template. This file should only ever have requires and nothing else.

### 6.2 Localization (Polylang)

No built-in polylang support at the moment. Will be added in later versions.

## 7. Gutenberg and Classic Editor

Koutabase supports both Gutenberg and Classic Editor though Gutenberg is preferred.

## 8. Menus

By default the starter theme has two menu locations: Main Navigation and Footer Navigation.

### 8.1 Main Navigation

Theme location: `main-nav`

Theme's main navigation in header that is build to handle multiple levels.

Usage `<?php koutabase_main_nav(); ?>`

Custom walker included in `included/classes/class-koutabase-walker-nav-menu.php`

### 8.2 Footer menu

Theme location: `footer-nav`

Optional menu in the site footer.

Usage `<?php koutabase_footer_nav(); ?>`

## 9. Editorconfig

Theme has an `.editorconfig` file that sets your code editor settings accordingly. Never used Editorconfig? [Download the extension](http://editorconfig.org/#download) to your editor. The settings will automatically be applied when you edit code when you have the extension.

Our settings:
```
indent_style = tab
end_of_line = lf
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
```

## 10. PHP CodeSniffer

PHP CodeSniffer is used to enforce our PHP coding standards.

`phpcs.xml` defines our relaxed version of [WordPress](https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/) coding standards.

## 11. Site specific plugin

It is recommended to create also site specific plugin to store custom post types, custom taxonomies, shortcodes, ACF fields etc information architecture. There is nothing stopping you from defining them into theme, but we find it better to isolate information architecture outside of theme as those post types and taxonomies will remain when theme gets rebuild.
