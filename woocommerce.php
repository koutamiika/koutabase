<?php
/**
 * Default Page Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();

?>

<?php get_template_part( 'template-parts/banner' ); // Banner ?>

<div id="inner-content" class="container">

	<div class="row <?php echo esc_attr( $kb_shop_layout_class['row'] ); ?>">

		<div id="main" class="<?php echo esc_attr( $kb_shop_layout_class['main'] ); ?> clearfix" role="main">

		<?php
		/**
		 * Koutabase After Page #main ends
		 */
		do_action( 'koutabase_after_page_main_begin' );
		?>

		<?php woocommerce_breadcrumb(); ?>

		<?php woocommerce_content(); ?>

		<?php
		/**
		 * Koutabase Before Page #main ends
		 */
		do_action( 'koutabase_before_page_main_end' );
		?>

		</div><?php // END #main ?>

		<?php get_sidebar( 'shop' ); ?>

	</div>

</div><?php // END #inner-content ?>

<?php
get_footer();
