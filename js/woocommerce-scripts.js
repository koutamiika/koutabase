jQuery(document).ready(function ($) {

	$('.variable-item').on("click", function () {

		var parent = $(this).parent();

		$(this).addClass('selected');
		$(this).siblings().removeClass('selected');
		$('#' + parent.attr('data-attribute_name')).val($(this).attr('data-value'));
		$('.variations select').trigger('change');

	});

	$('div.woocommerce').on('click', 'input.qty', function () {
		$("[name='update_cart']").trigger("click");
	});

})