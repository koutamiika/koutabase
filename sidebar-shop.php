<?php
/**
 * Sidebar Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

global $kb_shop_sidebar_class;

if ( is_active_sidebar( 'shop-left-sidebar' ) ) :
	?>

	<div id="left-sidebar" class="sidebar <?php echo esc_html( $kb_shop_sidebar_class['left'] ); ?>" role="complementary" <?php koutabase_schema_markup( 'sidebar', true ); ?>>
		<?php dynamic_sidebar( 'shop-left-sidebar' ); ?>
	</div>

	<?php
endif;

if ( is_active_sidebar( 'shop-right-sidebar' ) ) :
	?>

	<div id="right-sidebar" class="sidebar <?php echo esc_html( $kb_shop_sidebar_class['right'] ); ?>" role="complementary" <?php koutabase_schema_markup( 'sidebar', true ); ?>>
		<?php dynamic_sidebar( 'shop-right-sidebar' ); ?>
	</div>

	<?php
endif;
