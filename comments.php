<?php
/**
 * Comments Template
 *
 * The area of the page that contains both current comments and the comment form.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

/**
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// if you delete this the sky will fall on your head
	if ( comments_open() ) :
		?>

		<section class="respond-form">

			<?php
			// If registration required and not logged in
			if ( get_option( 'comment_registration' ) && ! is_user_logged_in() ) :
				?>

				<div class="alert help">
					<?php // translators: %1$s opening <a> tag to login page. %2$s closing <a> tag. ?>
					<p><?php printf( __( 'Sinun tulee olla %1$skirjautunut sisään%2$s kommentoidaksesi.', 'koutabase' ), '<a href="<?php echo wp_login_url( get_permalink() ); ?>">', '</a>' ); ?></p>
				</div>

			<?php else : ?>

				<?php comment_form(); ?>

			<?php endif; ?>

		</section>

		<?php
	endif;
	?>

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>

		<h3 class="h2 comments-title">
			<?php
			$_s_comment_count = get_comments_number();
			if ( '1' === $_s_comment_count ) {
				printf(
					/* translators: 1: title. */
					esc_html__( 'Yksi kommentti: &ldquo;%1$s&rdquo;', 'koutabase' ),
					'<span>' . esc_html( get_the_title() ) . '</span>'
				);
			} else {
				printf(
					/* translators: 1: comment count number, 2: title. */
					esc_html( _nx( '%1$s kommenttia: &ldquo;%2$s&rdquo;', '%1$s kommenttia: &ldquo;%2$s&rdquo;', $_s_comment_count, 'comments title', 'koutabase' ) ),
					number_format_i18n( $_s_comment_count ),
					'<span>' . esc_html( get_the_title() ) . '</span>'
				);
			}
			?>
		</h3>

		<?php
			the_comments_navigation( array(
				'prev_text' => '&larr; Vanhemmat kommentit',
				'next_text' => '&rarr; Uudemmat kommentit',
			) );
		?>

		<ol class="commentlist">
			<?php
				wp_list_comments( array(
					'type'     => 'comment',
					'style'	   => 'ol',
					'callback' => 'koutabase_comments',
				) );
			?>
		</ol>

		<?php
			the_comments_navigation( array(
				'prev_text' => '&larr; Vanhemmat kommentit',
				'next_text' => '&rarr; Uudemmat kommentit',
			) );
		?>

		<?php
		// this is displayed if there are no comments so far
	else :
		?>

		<?php if ( ! comments_open() && '0' !== get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

			<p class="nocomments"><?php esc_html_e( 'Kommentointi on suljettu.', 'koutabase' ); ?></p>

		<?php endif; ?>

		<?php
	endif;
	?>

</div>
