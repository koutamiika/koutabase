<?php
/**
 * Search Results Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();
?>

<?php get_template_part( 'template-parts/banner' ); // Banner. ?>

<div id="inner-content" class="container">

	<div id="main" class="clearfix" <?php koutabase_schema_markup( 'main', true ); ?>>

		<?php if ( have_posts() ) :	?>

			<header class="page-header">

				<?php get_search_form(); ?>

			</header>

			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'article' ); // WordPress loop article. ?>

			<?php endwhile; ?>

				<?php get_template_part( 'template-parts/pager' ); // WordPress template pager/pagination. ?>

			<?php else : ?>

				<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

		<?php endif; ?>

	</div><?php // END #main. ?>

</div><?php // END #inner-content. ?>

<?php
get_footer();
