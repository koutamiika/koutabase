<?php
/**
 * Footer Template
 *
 * Contains the closing of the "main" div and all content after.
 *
 * @package Koutabase
 */

?>
			<?php do_action( 'koutabase_before_content_end' ); ?>

		</div><?php // END #content. ?>

		<footer id="colophon" class="footer" role="contentinfo">

			<div id="inner-footer">

				<?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) : ?>

				<div id="footer-sidebar-1" class="sidebar clearfix" role="complementary">
					<?php dynamic_sidebar( 'footer-sidebar-1' ); ?>
				</div>

				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) : ?>

				<div id="footer-sidebar-2" class="sidebar clearfix" role="complementary">
					<?php dynamic_sidebar( 'footer-sidebar-2' ); ?>
				</div>

				<?php endif; ?>

				<nav role="navigation" aria-label="<?php esc_attr_e( 'Footer Navigation', 'koutabase' ); ?>">
					<?php koutabase_footer_nav(); ?>
				</nav>

				<?php // translators: %1$s Year. %2$s Site name. ?>
				<p class="source-org copyright"><?php echo sprintf( esc_html__( '&copy; %1$s %2$s.', 'koutabase' ), esc_html( date( 'Y' ) ), esc_html( get_bloginfo( 'name' ) ) ); ?>  <?php the_privacy_policy_link(); ?></p>

			</div>

		</footer>

	</div><?php // END #container. ?>

<?php wp_footer(); ?>

</body>
</html>
