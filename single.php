<?php
/**
 * Single Posts Template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();
?>

<?php get_template_part( 'template-parts/banner' ); // Banner. ?>

<div id="inner-content">

	<?php do_action( 'koutabase_after_single_inner_content_begin' ); ?>

	<div id="main" class="clearfix" <?php koutabase_schema_markup( 'main', true ); ?>>

	<?php
	/**
	 * Koutabase After Single #main begin
	 */
	do_action( 'koutabase_after_single_main_begin' );
	?>

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single-article' ); // Single article content. ?>

			<?php
			// Previous/next post navigation.
			the_post_navigation(
				array(
					'next_text' => '<p class="meta-nav">' . esc_html__( 'Seuraava artikkeli', 'koutabase' ) . ' &rarr;</p><p class="post-title">%title</p>',
					'prev_text' => '<p class="meta-nav">&larr; ' . esc_html__( 'Edellinen artikkeli', 'koutabase' ) . '</p><p class="post-title">%title</p>',
				)
			);
			?>

		<?php endwhile; ?>

	<?php else : ?>

		<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

	<?php endif; ?>

	<?php
	/**
	 * Koutabase Before Single #main ends
	 *
	 * @hooked koutabase_page_navigation - 10
	 */
	do_action( 'koutabase_before_single_main_end' );
	?>

	</div><?php // END #main. ?>

	<?php
	/**
	 * Koutabase Before Single #inner-content ends
	 */
	do_action( 'koutabase_before_single_inner_content_end' );
	?>

</div><?php // END #inner-content. ?>

<?php
get_footer();
