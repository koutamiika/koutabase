<?php
/**
 * Page for Posts Template as defined in Settings->Reading
 *
 * Used to display blog archive.
 *
 * @see http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();
?>


<?php get_template_part( 'template-parts/banner' ); // Banner. ?>

<div id="inner-content" class="container">

	<div id="main" class="clearfix" role="main" <?php koutabase_schema_markup( 'blog', true ); ?>>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'article' ); // WordPress loop article. ?>

			<?php endwhile; ?>

			<?php get_template_part( 'template-parts/pager' ); // WordPress template pager/pagination. ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

		<?php endif; ?>

	</div><?php // END #main ?>

</div><?php // END #inner-content ?>

<?php
get_footer();
