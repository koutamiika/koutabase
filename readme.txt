KoutaBase Theme
=======

KoutaBase theme is started theme for most Kouta developed WordPress themes. KoutaBase is based on Scaffolding by Hall.

Developed by Kouta Media Oy
https://www.koutamedia.fi/

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Submit Bugs & or Fixes:
https://bitbucket.org/koutamiika/koutabase/issues

## Installation

Run

`npm install gulp@3.9.1`
`npm install gulp-compass --save-dev & npm i browser-sync --save`
`npm install --save wp-cli`

After dependencies has been installed run

`gulp`