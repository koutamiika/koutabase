<?php
/**
 * Search related functions.
 *
 * @package Koutabase
 */

/**
 * Filter posts from query that are set to 'noindex'
 *
 * This function is dependent on Yoast SEO Plugin.
 *
 * @param object $query WP_query object.
 * @since Koutabase 1.1
 */
function koutabase_noindex_filter( $query ) {
	if ( ! is_admin() && $query->is_search() && defined( 'WPSEO_VERSION' ) ) {
		$meta_query = $query->get( 'meta_query' );
		if ( is_array( $meta_query ) ) {
			$meta_query[] = array(
				'key'     => '_yoast_wpseo_meta-robots-noindex',
				'compare' => 'NOT EXISTS',
			);
			$query->set( 'meta_query', $meta_query );
		} else {
			$meta_query = array(
				array(
					'key'     => '_yoast_wpseo_meta-robots-noindex',
					'compare' => 'NOT EXISTS',
				),
			);
			$query->set( 'meta_query', $meta_query );
		}
	}
	return $query;
}
add_action( 'pre_get_posts', 'koutabase_noindex_filter' );

/**
 * Return the search results page even if the query is empty
 *
 * @link http://vinayp.com.np/how-to-show-blank-search-on-wordpress
 *
 * @since Koutabase 1.0
 *
 * @param WP_Query $query Current WordPress Query.
 */
function koutabase_make_blank_search( $query ) {
	if ( ! is_admin() ) {
		global $wp_query;
		if ( isset( $_GET['s'] ) && '' == $_GET['s'] ) {  // if search parameter is blank, do not return false.
			$wp_query->set( 's', ' ' );
			$wp_query->is_search = true;
		}
		return $query;
	}
}
add_action( 'pre_get_posts', 'koutabase_make_blank_search' );
