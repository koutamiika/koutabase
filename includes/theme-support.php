<?php
/**
 * Declare theme supports
 *
 * @package Koutabase
 */

/**
 * Add WP3+ functions and theme support
 *
 * Function called in koutabase_build() in base-functions.php.
 *
 * @see koutabase_custom_headers_callback
 *
 * @since Koutabase 1.0
 */
function koutabase_theme_support() {

	// Make theme available for translation.
	load_theme_textdomain( 'koutabase', get_template_directory() . '/languages' );

	// Support for thumbnails.
	add_theme_support( 'post-thumbnails' );

	// Support for RSS.
	add_theme_support( 'automatic-feed-links' );

	// Support for Custom Logo.
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 60,
			'width'       => 150,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title' ),
		)
	);

	// Support for custom headers.
	add_theme_support(
		'custom-header',
		array(
			'default-image'          => '',
			'random-default'         => false,
			'width'                  => 1800,
			'height'                 => 350,
			'flex-height'            => true,
			'flex-width'             => true,
			'video'                  => true,
			'default-text-color'     => 'ffffff',
			'header-text'            => false,
			'uploads'                => true,
			'video-active-callback'  => true,
			'wp-head-callback'       => '',
			'admin-head-callback'    => '',
			'admin-preview-callback' => '',
		)
	);

	// HTML5.
	add_theme_support(
		'html5',
		array(
			'comment-list',
			'comment-form',
			'search-form',
			'gallery',
			'caption',
		)
	);

	// Title Tag.
	add_theme_support( 'title-tag' );

	// Additional theme supports.
	add_theme_support( 'custom-background' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'align-wide' );
	add_theme_support( 'custom-units' );
	add_theme_support( 'custom-spacing' );
	add_theme_support( 'custom-line-height' );
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	// Add styles for use in visual editor.
	add_theme_support( 'editor-styles' );
	add_editor_style( 'css/editor-styles.css' );

} // end koutabase_theme_support()
