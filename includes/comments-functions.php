<?php
/**
 * Comments related function
 *
 * @package Koutabase
 */

/**
 * Comment Layout
 *
 * @param string $comment Comment.
 * @param array  $args Arguments.
 * @param int    $depth Comment depth.
 *
 * @since Koutabase 1.0
 */
function koutabase_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	$tag                = ( 'div' === $args['style'] ) ? 'div' : 'li';
	?>
	<<?php echo esc_attr( $tag ); ?> <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix" >
			<header class="comment-author vcard">
				<?php
				if ( 0 !== $args['avatar_size'] ) {
					echo get_avatar( $comment, $args['avatar_size'], '', get_comment_author() );
				}
				?>
				<?php // translators: Comment author. ?>
				<?php printf( __( '<cite class="fn">%s</cite>', 'koutabase' ), get_comment_author_link() ); ?>
				<time datetime="<?php echo esc_attr( comment_time( 'Y-m-d' ) ); ?>"><a class="comment-date-link" href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>"><?php comment_time( __( 'd.m.Y', 'koutabase' ) ); ?> </a> <?php edit_comment_link( __( '(Muokkaa)', 'koutabase' ), '<em>', '</em>' ); ?></time>
			</header>
			<?php if ( '0' === $comment->comment_approved ) : ?>
				<div class="alert info">
					<p><?php esc_html_e( 'Kommenttisi odottaa moderointia.', 'koutabase' ); ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix" <?php koutabase_schema_markup( 'comment', true ); ?>>
				<?php comment_text(); ?>
			</section>
			<?php
			comment_reply_link(
				array_merge(
					$args,
					array(
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
					)
				)
			);
			?>
		</article>
	<?php // </li> or </div> is added by WordPress automatically. ?>
	<?php
} // end koutabase_comments().
