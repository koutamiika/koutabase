<?php
/**
 * Register all sidebars here.
 *
 * @package Koutabase
 */

/**
 * Sidebars & Widgets Areas
 *
 * Two sidebars registered - left and right.
 * Define additional sidebars here.
 *
 * @since Koutabase 1.0
 */
function koutabase_register_sidebars() {
	register_sidebar(
		array(
			'id'            => 'left-sidebar',
			'name'          => __( 'Vasen Vimpainalue', 'koutabase' ),
			'description'   => __( 'Vimpainalue sivuston vasemmalla puolella.', 'koutabase' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>',
		)
	);

	register_sidebar(
		array(
			'id'            => 'right-sidebar',
			'name'          => __( 'Oikea Vimpainalue', 'koutabase' ),
			'description'   => __( 'Vimpainalue sivuston oikealla puolella', 'koutabase' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>',
		)
	);

	register_sidebar(
		array(
			'id'            => 'footer-sidebar-1',
			'name'          => __( 'Footer Vimpainalue 1', 'koutabase' ),
			'description'   => __( 'Vimpainalue sivuston alatunnisteessa.', 'koutabase' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>',
		)
	);

	register_sidebar(
		array(
			'id'            => 'footer-sidebar-2',
			'name'          => __( 'Footer Vimpainalue 2', 'koutabase' ),
			'description'   => __( 'Vimpainalue sivuston alatunnisteessa. Näkyy Footer Vimpainalue 1:n alapuolella.', 'koutabase' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>',
		)
	);
} // end koutabase_register_sidebars().
