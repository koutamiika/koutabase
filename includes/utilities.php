<?php
/**
 * Utility functions
 *
 * @package Koutabase
 */

/**
 * Removes the annoying […] to a Read More link
 *
 * @since Koutabase 1.0
 *
 * @global post
 *
 * @param string $more Initial more text.
 *
 * @return string new more text.
 */
function koutabase_excerpt_more( $more ) {
	global $post;
	// translators: %1$s URL of the post. %2$s Post title.
	return sprintf( __( '&hellip; <a class="read-more" href="%1$s">Lue lisää &raquo;</a>', 'koutabase' ), get_permalink( $post->ID ) );
}

/**
 * Modifies author post link which just returns the link
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 *
 * @since Koutabase 1.0
 *
 * @global authordata
 */
function koutabase_get_the_author_posts_link() {
	global $authordata;
	if ( ! is_object( $authordata ) ) {
		return false;
	}
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		// translators: author.
		esc_attr( sprintf( __( 'Kirjoittajan %s artikkelit:', 'koutabase' ), get_the_author() ) ), // No further l10n needed, core will take care of this one.
		get_the_author()
	);
	return $link;
}

/**
 * Set grid classes based on sidebars
 *
 * @param string $type layout type.
 *
 * @since Koutabase 1.0
 **/
function koutabase_set_layout_classes( $type ) {

	if ( '' === $type || ( 'content' !== $type && 'sidebar' !== $type && 'shop' !== $type && 'shop-sidebar' !== $type ) ) {
		return;
	}

	if ( 'content' === $type ) {

		$class = array(
			'row'  => 'row-main no-sidebars',
			'main' => 'col-12',
		);

		// Test for active sidebars to set the main content width.
		if ( is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) {
			if ( is_page_template( 'templates/left-sidebar.php' ) ) {
				$class['row']  = 'row-main has-left-sidebar';
				$class['main'] = 'col-lg-9 order-lg-2';
			} elseif ( is_page_template( 'templates/right-sidebar.php' ) ) {
				$class['row']  = 'row-main has-right-sidebar';
				$class['main'] = 'col-lg-9 order-lg-1';
			} else {
				$class['row']  = 'row-main has-both-sidebars';
				$class['main'] = 'col-lg-6 order-lg-2';
			}
		} elseif ( is_active_sidebar( 'left-sidebar' ) && ! is_active_sidebar( 'right-sidebar' ) ) {
			$class['row']  = 'row-main has-left-sidebar';
			$class['main'] = 'col-lg-9 order-lg-2';
		} elseif ( ! is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) {
			$class['row']  = 'row-main has-right-sidebar';
			$class['main'] = 'col-lg-9 order-lg-1';
		}
	} elseif ( 'shop' === $type ) {

		$class = array(
			'row'  => 'row-main no-sidebars',
			'main' => 'col-12',
		);

		// Test for active sidebars to set the main content width.
		if ( is_active_sidebar( 'shop-left-sidebar' ) && is_active_sidebar( 'shop-right-sidebar' ) ) {
			if ( is_page_template( 'templates/left-sidebar.php' ) ) {
				$class['row']  = 'row-main has-left-sidebar';
				$class['main'] = 'col-lg-9 order-lg-2';
			} elseif ( is_page_template( 'templates/right-sidebar.php' ) ) {
				$class['row']  = 'row-main has-right-sidebar';
				$class['main'] = 'col-lg-9 order-lg-1';
			} else {
				$class['row']  = 'row-main has-both-sidebars';
				$class['main'] = 'col-lg-6 order-lg-2';
			}
		} elseif ( is_active_sidebar( 'shop-left-sidebar' ) && ! is_active_sidebar( 'shop-right-sidebar' ) ) {
			$class['row']  = 'row-main has-left-sidebar';
			$class['main'] = 'col-lg-9 order-lg-2';
		} elseif ( ! is_active_sidebar( 'shop-left-sidebar' ) && is_active_sidebar( 'shop-right-sidebar' ) ) {
			$class['row']  = 'row-main has-right-sidebar';
			$class['main'] = 'col-lg-9 order-lg-1';
		}
	} elseif ( 'sidebar' === $type ) {

		$class = array(
			'left'  => '',
			'right' => '',
		);

		// Test for active sidebars to set sidebar classes.
		if ( is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) {
			$class['left']  = 'order-lg-1 col-lg-3';
			$class['right'] = 'order-lg-3 col-lg-3';
		} elseif ( is_active_sidebar( 'left-sidebar' ) && ! is_active_sidebar( 'right-sidebar' ) ) {
			$class['left'] = 'col-lg-3 order-lg-1';
		} elseif ( ! is_active_sidebar( 'left-sidebar' ) && is_active_sidebar( 'right-sidebar' ) ) {
			$class['right'] = 'col-lg-3 order-lg-2';
		}
	} elseif ( 'shop-sidebar' === $type ) {

		$class = array(
			'left'  => '',
			'right' => '',
		);

		// Test for active sidebars to set sidebar classes.
		if ( is_active_sidebar( 'shop-left-sidebar' ) && is_active_sidebar( 'shop-right-sidebar' ) ) {
			$class['left']  = 'order-lg-1 col-lg-3';
			$class['right'] = 'order-lg-3 col-lg-3';
		} elseif ( is_active_sidebar( 'shop-left-sidebar' ) && ! is_active_sidebar( 'shop-right-sidebar' ) ) {
			$class['left'] = 'col-lg-3 order-lg-1';
		} elseif ( ! is_active_sidebar( 'shop-left-sidebar' ) && is_active_sidebar( 'shop-right-sidebar' ) ) {
			$class['right'] = 'col-lg-3 order-lg-2';
		}
	}

	return $class;
}


/**
 * Set globals for layout classes
 *
 * @since Koutabase 1.0
 */
function koutabase_layout_classes_globals() {
	if ( function_exists( 'koutabase_set_layout_classes' ) ) {
		$GLOBALS['kb_sidebar_class']      = koutabase_set_layout_classes( 'sidebar' );
		$GLOBALS['kb_layout_class']       = koutabase_set_layout_classes( 'content' );
		$GLOBALS['kb_shop_sidebar_class'] = koutabase_set_layout_classes( 'shop-sidebar' );
		$GLOBALS['kb_shop_layout_class']  = koutabase_set_layout_classes( 'shop' );
	} else {
		$GLOBALS['kb_sidebar_class'] = array(
			'left'  => '',
			'right' => '',
		);
		$GLOBALS['kb_layout_class']  = array(
			'row'  => 'row-main no-sidebars',
			'main' => 'col-12',
		);
	}
}
add_action( 'koutabase_after_content_begin', 'koutabase_layout_classes_globals', 0 );

/**
 * Schema.org additions
 *
 * @param string $type of the element.
 * @param string $echo if the result should be echoed.
 *
 * @return string Attribute.
 */
function koutabase_schema_markup( $type, $echo = false ) {

	if ( empty( $type ) ) {
		return false;
	}

	$disable = apply_filters( 'koutabase_schema_markup_disable', false );

	if ( true === $disable ) {
		return false;
	}

	$attributes = '';
	$attr       = array();

	switch ( $type ) {
		case 'body':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/WebPage';
			break;

		case 'header':
			$attr['role']      = 'banner';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/WPHeader';
			break;

		case 'nav':
			$attr['role']      = 'navigation';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/SiteNavigationElement';
			break;

		case 'title':
			$attr['itemprop'] = 'headline';
			break;

		case 'subtitle':
			$attr['itemprop'] = 'alternativeHeadline';
			break;

		case 'sidebar':
			$attr['role']      = 'complementary';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/WPSideBar';
			break;

		case 'footer':
			$attr['role']      = 'contentinfo';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/WPFooter';
			break;

		case 'main':
			$attr['role']     = 'main';
			$attr['itemprop'] = 'mainContentOfPage';

			if ( is_search() ) {
				$attr['itemtype'] = 'https://schema.org/SearchResultsPage';
			}
			break;

		case 'author':
			$attr['itemprop']  = 'author';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Person';
			break;

		case 'person':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Person';
			break;

		case 'comment':
			$attr['itemprop']  = 'comment';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/UserComments';
			break;

		case 'comment_author':
			$attr['itemprop']  = 'creator';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Person';
			break;

		case 'comment_author_link':
			$attr['itemprop']  = 'creator';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Person';
			$attr['rel']       = 'external nofollow';
			break;

		case 'comment_time':
			$attr['itemprop']  = 'commentTime';
			$attr['itemscope'] = 'itemscope';
			$attr['datetime']  = get_the_time( 'c' );
			break;

		case 'comment_text':
			$attr['itemprop'] = 'commentText';
			break;

		case 'author_box':
			$attr['itemprop']  = 'author';
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Person';
			break;

		case 'video':
			$attr['itemprop'] = 'video';
			$attr['itemtype'] = 'https://schema.org/VideoObject';
			break;

		case 'audio':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/AudioObject';
			break;

		case 'blog':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/Blog';
			break;

		case 'blogpost':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/BlogPosting';
			break;

		case 'article_body':
			$attr['itemprop'] = 'articleBody';
			break;

		case 'name':
			$attr['itemprop'] = 'name';
			break;

		case 'url':
			$attr['itemprop'] = 'url';
			break;

		case 'email':
			$attr['itemprop'] = 'email';
			break;

		case 'post_time':
			$attr['itemprop'] = 'datePublished';
			break;

		case 'post_content':
			$attr['itemprop'] = 'text';
			break;

		case 'creative_work':
			$attr['itemscope'] = 'itemscope';
			$attr['itemtype']  = 'https://schema.org/CreativeWork';
			break;

		case 'logo':
			$attr['itemprop'] = 'logo';
			break;
	}

	/**
	 * Filter to override or append attributes
	 *
	 * @var array
	 */
	$attr = apply_filters( 'koutabase_schema_markup_attributes', $attr );

	foreach ( $attr as $key => $value ) {
		$attributes .= $key . '="' . esc_attr( $value ) . '" ';
	}

	if ( $echo ) {
		echo '' . wp_kses_post( $attributes );
	}
	return $attributes;
}

if ( ! function_exists( 'koutabase_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 *
	 * @return void
	 */
	function koutabase_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() )
		);
		echo '<span class="posted-on">';
		printf(
			/* translators: %s: Publish date. */
			esc_html__( 'Julkaistu: %s', 'koutabase' ),
			$time_string // phpcs:ignore WordPress.Security.EscapeOutput
		);
		echo '</span>';
	}
}

if ( ! function_exists( 'koutabase_posted_by' ) ) {
	/**
	 * Prints HTML with meta information about theme author.
	 *
	 * @return void
	 */
	function koutabase_posted_by() {
		if ( ! get_the_author_meta( 'description' ) && post_type_supports( get_post_type(), 'author' ) ) {
			echo '<span class="byline">';
			printf(
				/* translators: %s: Author name. */
				esc_html__( 'Kirjoittaja: %s', 'koutabase' ),
				'<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" rel="author">' . esc_html( get_the_author() ) . '</a>'
			);
			echo '</span>';
		}
	}
}

/**
 * Build post meta
 */
function koutabase_post_meta() {
		// Early exit if not a post.
		if ( 'post' !== get_post_type() ) {
			return;
		}

		// Hide meta information on pages.
		if ( ! is_single() ) {

			if ( is_sticky() ) {
				echo '<p>' . esc_html_x( 'Suositeltu artikkeli', 'Label for sticky posts', 'koutabase' ) . '</p>';
			}

			// Posted on.
			koutabase_posted_on();

			// Edit post link.
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post. Only visible to screen readers. */
					esc_html__( 'Muokkaa %s', 'koutabase' ),
					'<span class="screen-reader-text">' . get_the_title() . '</span>'
				),
				'<span class="edit-link">',
				'</span><br>'
			);

			if ( has_category() || has_tag() ) {

				echo '<div class="post-taxonomies">';

				/* translators: Used between list items, there is a space after the comma. */
				$categories_list = get_the_category_list( __( ', ', 'koutabase' ) );
				if ( $categories_list ) {
					printf(
						/* translators: %s: List of categories. */
						'<span class="cat-links">' . esc_html__( 'Kategoria(t): %s', 'koutabase' ) . ' </span>',
						$categories_list // phpcs:ignore WordPress.Security.EscapeOutput
					);
				}

				/* translators: Used between list items, there is a space after the comma. */
				$tags_list = get_the_tag_list( '', __( ', ', 'koutabase' ) );
				if ( $tags_list ) {
					printf(
						/* translators: %s: List of tags. */
						'<span class="tags-links">' . esc_html__( 'Avainsanat: %s', 'koutabase' ) . '</span>',
						$tags_list // phpcs:ignore WordPress.Security.EscapeOutput
					);
				}
				echo '</div>';
			}
		} else {

			echo '<div class="posted-by">';
			// Posted on.
			koutabase_posted_on();
			// Posted by.
			koutabase_posted_by();
			// Edit post link.
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post. Only visible to screen readers. */
					esc_html__( 'Muokkaa %s', 'koutabase' ),
					'<span class="screen-reader-text">' . get_the_title() . '</span>'
				),
				'<span class="edit-link">',
				'</span>'
			);
			echo '</div>';

			if ( has_category() || has_tag() ) {

				echo '<div class="post-taxonomies">';

				/* translators: Used between list items, there is a space after the comma. */
				$categories_list = get_the_category_list( __( ', ', 'koutabase' ) );
				if ( $categories_list ) {
					printf(
						/* translators: %s: List of categories. */
						'<span class="cat-links">' . esc_html__( 'Kategoria(t): %s', 'koutabase' ) . ' </span>',
						$categories_list // phpcs:ignore WordPress.Security.EscapeOutput
					);
				}

				/* translators: Used between list items, there is a space after the comma. */
				$tags_list = get_the_tag_list( '', __( ', ', 'koutabase' ) );
				if ( $tags_list ) {
					printf(
						/* translators: %s: List of tags. */
						'<span class="tags-links">' . esc_html__( 'Avainsanat: %s', 'koutabase' ) . '</span>',
						$tags_list // phpcs:ignore WordPress.Security.EscapeOutput
					);
				}
				echo '</div>';
			}
		}
}
