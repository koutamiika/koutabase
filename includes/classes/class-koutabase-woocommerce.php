<?php
/**
 * WooCommerce related functionality
 *
 * @package Koutabase
 */

class Koutabase_WooCommerce {

	/**
	 * Construct
	 */
	public function __construct() {

		$this->woocommerce_filters();
		$this->remove_woocommerce_actions();
		$this->add_woocommerce_actions();

	}

	/**
	 * Remove default WooCommerce Actions here.
	 */
	public function remove_woocommerce_actions() {

		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 ); // Remote flash sale tag so we can move to another position
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 ); // Remote flash sale tag so we can move to another position
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 ); // Remote product meta so we can move to another position

		if ( ! get_theme_mod( 'show_add_to_cart' ) ) {
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 ); // Remove default add to cart button.
		}

	}

	/**
	 * Add WooCommerce actions here.
	 */
	public function add_woocommerce_actions() {
		add_action( 'after_setup_theme', array( $this, 'woocommerce_setup' ) ); // Declare WooCommerce compability and setup functionalities
		add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 0 );
		add_action( 'woocommerce_before_shop_loop', array( $this, 'woocommerce_product_columns_wrapper' ) ); // Add wrapper div for WC products
		add_action( 'woocommerce_after_shop_loop', array( $this, 'woocommerce_product_columns_wrapper_close' ) ); // Add wrapper div for WC products
		add_action( 'woocommerce_checkout_order_review', array( $this, 'checkout_protected_text' ), 99 ); // Add protected by SSL message after place order button
		add_action( 'woocommerce_before_shop_loop', array( $this, 'woocommerce_ordering_before' ), 0 ); // Add wrapper div for ordering
		add_action( 'woocommerce_before_shop_loop', array( $this, 'woocommerce_ordering_after' ), 35 ); // Add wrapper div for ordering
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'woocommerce_template_loop_second_product_thumbnail' ), 11 );
		add_action( 'woocommerce_share', array( $this, 'share_links' ), 10 );
		add_action( 'woocommerce_grouped_product_list_before_quantity', array( $this, 'woocommerce_grouped_product_thumbnail' ) );
		add_action( 'widgets_init', array( $this, 'register_shop_sidebars' ) );

	}

	/**
	 * Add WooCommerce filters here.
	 */
	public function woocommerce_filters() {
		add_filter( 'body_class', array( $this, 'woocommerce_active_body_class' ) ); // Add class to body when WooCommerce is active
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' ); // Disable default WooCommerce styles.
		add_filter( 'woocommerce_enqueue_styles', array( $this, 'koutabase_woocommerce_styles' ) ); // Enqueue our custom styles.
		add_filter( 'wp_enqueue_scripts', array( $this, 'koutabase_woocommerce_scripts' ) ); // Enqueue our custom styles.
		//add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'woocommerce_cart_link_fragment' ) );
		add_filter( 'woocommerce_variable_sale_price_html', array( $this, 'variation_price_format' ), 10, 2 );
		add_filter( 'woocommerce_variable_price_html', array( $this, 'variation_price_format' ), 10, 2 );
		add_filter( 'woocommerce_checkout_fields', array( $this, 'checkout_fields' ) );
		add_filter( 'woocommerce_breadcrumb_defaults', array( $this, 'change_breadcrumb_delimiter' ) );
		add_filter( 'woocommerce_order_button_text', array( $this, 'rename_place_order_button' ) );
		add_filter( 'woocommerce_registration_auth_new_customer', '__return_false' );
		add_filter( 'post_class', array( $this, 'product_has_gallery' ) );
	}

	/**
	 * Display protected text after checkout button.
	 */
	public static function checkout_protected_text() {
		echo '<p class="ssl-protected text-center"><i class="fas fa-lock"></i> ' . esc_html__( 'Tilaus suojattu SSL tekniikalla.', 'koutabase' ) . '</p>';
	}

	/**
	 * Register our custom WooCommerce styles
	 */
	public function koutabase_woocommerce_styles() {
		wp_enqueue_style( 'koutabase-woo-styles', get_template_directory_uri() . '/css/plugins/woocommerce/woocommerce.css', array(), KOUTABASE_THEME_VERSION );
	}

	/**
	 * Register our custom WooCommerce js
	 */
	public function koutabase_woocommerce_scripts() {
		wp_enqueue_script( 'koutabase-woo-js', get_template_directory_uri() . '/js/woocommerce-scripts.js', array( 'jquery' ), KOUTABASE_THEME_VERSION, true );
	}

	/**
	 * WooCommerce setup function.
	 *
	 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
	 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)-in-3.0.0
	 *
	 * @return void
	 */
	public function woocommerce_setup() {

		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-slider' );

	}

	/**
	 * Add 'woocommerce-active' class to the body tag.
	 *
	 * @param array $classes CSS classes applied to the body tag.
	 *
	 * @return array $classes modified to include 'woocommerce-active' class.
	 */
	public function woocommerce_active_body_class( $classes ) {
		$classes[] = 'woocommerce-active';

		return $classes;
	}

	/**
	 * Change the breadcrumb separator
	 *
	 * @param string $defaults string of delimiter.
	 *
	 * @return string $defaults string of delimiter.
	 */
	public function change_breadcrumb_delimiter( $defaults ) {
		// Change the breadcrumb delimeter from '/' to '>'.
		$defaults['delimiter'] = '<span class="delimiter">&raquo;</span>';
		return $defaults;
	}

	/**
	 * Before Ordering.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	public function woocommerce_ordering_before() {
		?>
		<div class="shop-content clearfix">
		<?php
	}

	/**
	 * After Ordering.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	public function woocommerce_ordering_after() {
		?>
		</div><!-- .ordering-wrapper -->
		<?php
	}

	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	public function woocommerce_wrapper_before() {
		?>
		<div class="row <?php echo esc_attr( $kb_layout_class['row'] ); ?>">
		<div id="inner-content" class="col-md-9 order-md-1 content-area">
		<?php
	}

	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	public function woocommerce_wrapper_after() {
		?>
		</div><!-- #inner-content -->
		</div><!-- .row -->
		<?php
	}

	/**
	 * Product columns wrapper.
	 *
	 * @return void
	 */
	public function woocommerce_product_columns_wrapper() {
		echo '<div class="ordering-wrapper">';
	}

	/**
	 * Product columns wrapper close.
	 *
	 * @return void
	 */
	public function woocommerce_product_columns_wrapper_close() {
		echo '</div>';
	}

	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 *
	 * @return array Fragments to refresh via AJAX.
	 */
	public function woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		$this->koutabase_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		ob_start();
		koutabase_handheld_footer_bar_cart_link();
		$fragments['a.footer-cart-contents'] = ob_get_clean();

		return $fragments;
	}

	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	public static function koutabase_woocommerce_cart_link() {
		?>
			<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'Näytä ostoskori', 'koutabase' ); ?>">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/icons/outline-shopping_cart-24px.svg" alt="Cart">
				<?php /* translators: number of items in the mini cart. */ ?>
				<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d', '%d', WC()->cart->get_cart_contents_count(), 'koutabase' ), WC()->cart->get_cart_contents_count() ) ); ?></span><span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span>
			</a>
		<?php
	}

	/**
	 * Change variable product price to show alk. price instead of price range.
	 *
	 * @param array  $price Get products prices.
	 * @param object $product Product object.
	 */
	public function variation_price_format( $price, $product ) {
		// Main Price.
		$prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
		// translators: %1$s lowest price.
		$price = $prices[0] !== $prices[1] ? sprintf( __( 'alk. %1$s', 'koutabase' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

		// Sale Price.
		$prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
		sort( $prices );
		// translators: %1$s lowest price.
		$saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'alk. %1$s', 'koutabase' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

		if ( $price !== $saleprice ) {
			$price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
		}

		return $price;
	}

	/**
	 * Remove country from checkout form
	 *
	 * @param array $fields array of checkout form fields.
	 */
	public function checkout_fields( $fields ) {
		$fields['billing']['billing_first_name']['placeholder'] = 'Etunimi';
		$fields['billing']['billing_last_name']['placeholder']  = 'Sukunimi';
		$fields['billing']['billing_company']['placeholder']    = 'Yritys (valinnainen)';
		$fields['billing']['billing_postcode']['placeholder']   = 'Postinumero';
		$fields['billing']['billing_phone']['placeholder']      = 'Puhelinnumero';
		$fields['billing']['billing_city']['placeholder']       = 'Postitoimipaikka';
		$fields['billing']['billing_email']['placeholder']      = 'Sähköpostiosoite';

		$fields['shipping']['shipping_first_name']['placeholder'] = 'Etunimi';
		$fields['shipping']['shipping_last_name']['placeholder']  = 'Sukunimi';
		$fields['shipping']['shipping_company']['placeholder']    = 'Yritys (valinnainen)';
		$fields['shipping']['shipping_postcode']['placeholder']   = 'Postinumero';
		$fields['shipping']['shipping_phone']['placeholder']      = 'Puhelinnumero';
		$fields['shipping']['shipping_city']['placeholder']       = 'Postitoimipaikka';

		return $fields;
	}

	/**
	 * Change "place order" button text.
	 */
	public function rename_place_order_button() {
		return 'Vahvista tilaus &rarr;';
	}

	public function product_has_gallery( $classes ) {
		global $product;

		$post_type = get_post_type( get_the_ID() );

		if ( ! is_admin() ) {

			if ( 'product' === $post_type ) {

				$attachment_ids = $this->get_gallery_image_ids( $product );

				if ( $attachment_ids ) {
					$classes[] = 'koutabase-has-gallery';
				}
			}
		}

		return $classes;
	}

	/**
	 * Frontend functions
	 */
	public function woocommerce_template_loop_second_product_thumbnail() {
		global $product, $woocommerce;

		$attachment_ids = $this->get_gallery_image_ids( $product );

		if ( $attachment_ids ) {
			$attachment_ids     = array_values( $attachment_ids );
			$secondary_image_id = $attachment_ids['0'];

			$secondary_image_alt   = get_post_meta( $secondary_image_id, '_wp_attachment_image_alt', true );
			$secondary_image_title = get_the_title( $secondary_image_id );

			echo wp_get_attachment_image(
				$secondary_image_id,
				'shop_catalog',
				'',
				array(
					'class' => 'secondary-image attachment-shop-catalog wp-post-image wp-post-image--secondary',
					'alt'   => $secondary_image_alt,
					'title' => $secondary_image_title,
				)
			);
		}
	}


	/**
	 * WooCommerce Compatibility Functions
	 */
	public function get_gallery_image_ids( $product ) {
		if ( ! is_a( $product, 'WC_Product' ) ) {
			return;
		}

		if ( is_callable( 'WC_Product::get_gallery_image_ids' ) ) {
			return $product->get_gallery_image_ids();
		} else {
			return $product->get_gallery_attachment_ids();
		}
	}

	public function share_links() {
		echo '<iframe src="https://www.facebook.com/plugins/share_button.php?href=' . get_the_permalink() . '&layout=button_count&size=small&width=96&height=20&appId" width="96" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>';
	}

	public function woocommerce_grouped_product_thumbnail( $product ) {
		$image_size    = array( 100, 100 );  // array( width, height ) image size in pixel
		$attachment_id = get_post_meta( $product->get_id(), '_thumbnail_id', true );
		?>
		<td class="label">
			<?php echo wp_get_attachment_image( $attachment_id, 'medium' ); ?>
		</td>
		<?php
	}

	/**
	 * Register left and right sidebars for shop
	 */
	public function register_shop_sidebars() {
		register_sidebar(
			array(
				'id'            => 'shop-left-sidebar',
				'name'          => __( 'Kaupan Vasen Vimpainalue', 'koutabase' ),
				'description'   => __( 'Vimpainalue Kaupan vasemmalla puolella.', 'koutabase' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widgettitle">',
				'after_title'   => '</h4>',
			)
		);

		register_sidebar(
			array(
				'id'            => 'shop-right-sidebar',
				'name'          => __( 'Kaupan Oikea Vimpainalue', 'koutabase' ),
				'description'   => __( 'Vimpainalue Kaupan oikealla puolella.', 'koutabase' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widgettitle">',
				'after_title'   => '</h4>',
			)
		);
	}

}

new Koutabase_WooCommerce();
