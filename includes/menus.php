<?php
/**
 * Add any additional menus here.
 *
 * @see koutabase_walker_nav_menu
 * @since Koutabase 1.0
 * @package Koutabase
 */

/**
 * Register menus
 */
function koutabase_register_menus() {
	// Register WP3+ menus.
	register_nav_menus(
		array(
			'main-nav'   => __( 'Ensisijainen valikko', 'koutabase' ),    // main nav in header.
			'mobile-nav' => __( 'Mobiilivalikko', 'koutabase' ),    // mobile nav in header.
			'footer-nav' => __( 'Footer valikko', 'koutabase' ),   // secondary nav in footer.
		)
	);
}
add_action( 'after_setup_theme', 'koutabase_register_menus' );

/**
 * Main navigation menu
 */
function koutabase_main_nav() {
	wp_nav_menu(
		array(
			'container'       => '',
			'container_class' => '',
			'menu'            => '',
			'menu_class'      => 'menu main-menu desktop-menu',
			'theme_location'  => 'main-nav',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'fallback_cb'     => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'echo'            => true,
			'walker'          => new Koutabase_Walker_Nav_Menu(),
		)
	);
} // end koutabase_main_nav()

/**
 * Mobile navigation menu
 */
function koutabase_mobile_nav() {
	wp_nav_menu(
		array(
			'container'       => '',
			'container_class' => '',
			'menu'            => '',
			'menu_class'      => 'menu main-menu mobile-menu',
			'theme_location'  => 'mobile-nav',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'fallback_cb'     => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'echo'            => true,
			'walker'          => new Koutabase_Walker_Nav_Menu(),
		)
	);
} // end koutabase_mobile_nav()

/**
 * Footer menu (should you choose to use one)
 */
function koutabase_footer_nav() {
	wp_nav_menu(
		array(
			'container'       => '',
			'container_class' => '',
			'menu'            => '',
			'menu_class'      => 'menu footer-menu',
			'theme_location'  => 'footer-nav',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'depth'           => 0,
			'echo'            => true,
			'fallback_cb'     => '__return_false',
		)
	);
} // end koutabase_footer_nav()
