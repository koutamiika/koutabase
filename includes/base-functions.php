<?php
/**
 * Koutabase Stock Functions
 *
 * Included stock functions. Custom functions go in functions.php to facilitate future updates if necessary.
 *
 * @link https://bitbucket.org/koutamiika/koutabase/src/master/
 * @link https://codex.wordpress.org/Theme_Development
 *
 * @package Koutabase
 *
 * Table of Contents
 *
 * 1.0 - Initiating Koutabase
 * 2.0 - Cleaning Up wp_head
 * 3.0 - Front-End Improvements
 *    3.1 - Add attributes to next post link
 *    3.2 - Add attributes to previous post link
 *    3.3 - Add title attribute to wp_list_pages
 *    3.4 - Allow for blank search
 * 4.0 - Page Navi
 * 5.0 - Custom Login
 *    5.1 - Add styles to login page
 *    5.2 - Change logo link
 *    5.3 - Change alt attribute on logo
 * 6.0 - Visitor UX Functions
 *    6.1 - Remove p tags from images
 */

/************************************
 * 1.0 - INITIATING KOUTABASE
 ************************************/

/**
 * Koutabase Setup
 *
 * All of these functions are defined below or in functions.php.
 *
 * @since Koutabase 1.0
 */
function koutabase_build() {
	add_filter( 'wp_head', 'koutabase_remove_wp_widget_recent_comments_style', 1 ); // remove pesky injected css for recent comments widget.
	add_action( 'wp_head', 'koutabase_remove_recent_comments_style', 1 );           // clean up comment styles in the head.
	add_action( 'wp_enqueue_scripts', 'koutabase_scripts_and_styles', 999 );        // enqueue base scripts and styles.
	koutabase_add_image_sizes();                                                    // add additional image sizes.
	koutabase_theme_support();                                                      // launching this stuff after theme setup.
	add_action( 'widgets_init', 'koutabase_register_sidebars' );                    // adding sidebars to WordPress (these are created in functions.php).
	add_filter( 'the_content', 'koutabase_filter_ptags_on_images' );                // cleaning up random code around images.
	add_filter( 'excerpt_more', 'koutabase_excerpt_more' );                         // cleaning up excerpt.
	add_filter( 'koutabase_show_page_title', '__return_null' );
	add_filter( 'xmlrpc_enabled', '__return_false' ); 	                            // Disable XML-RPC
}
add_action( 'after_setup_theme', 'koutabase_build', 16 );


/************************************
 * 2.0 - CLEANING UP WP_HEAD
 *************************************/

/**
 * Remove injected CSS for recent comments widget
 *
 * This function is called in koutabase_build().
 *
 * @since Koutabase 1.0
 */
function koutabase_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

/**
 * Append to <head>
 */
add_action(
	'wp_head',
	function() {
		// replace class no-js with js in html tag.
		echo "<script>(function(d){d.className = d.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
	}
);

/**
 * Remove injected CSS from recent comments widget
 *
 * This function is called in koutabase_build().
 *
 * @since Koutabase 1.0
 */
function koutabase_remove_recent_comments_style() {
	global $wp_widget_factory;
	if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
		remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
	}
}


/************************************
 * 3.0 - FRONT-END IMPROVEMENTS
 *    3.1 - Add attributes to next post link
 *    3.2 - Add attributes to previous post link
 *    3.3 - Add title attribute to wp_list_pages
 *    3.4 - Allow for blank search
 *************************************/

/**
 * Add rel and title attribute to next pagination link
 *
 * @since Koutabase 1.0
 *
 * @param string $attr Previous "Next Page" rel attribute.
 *
 * @return string New "Next Page rel attribute.
 */
function koutabase_get_next_posts_link_attributes( $attr ) {
	$attr = 'rel="next" title="Näytä seuraava sivu"';
	return $attr;
}
add_filter( 'next_posts_link_attributes', 'koutabase_get_next_posts_link_attributes' );

/**
 * Add rel and title attribute to prev pagination link
 *
 * @since Koutabase 1.0
 *
 * @param string $attr Previous "Previous Page" rel attribute.
 *
 * @return string New "Previous Page rel attribute.
 */
function koutabase_get_previous_posts_link_attributes( $attr ) {
	$attr = 'rel="prev" title="Näytä edellinen sivu"';
	return $attr;
}
add_filter( 'previous_posts_link_attributes', 'koutabase_get_previous_posts_link_attributes' );

/**
 * Add page title attribute to wp_list_pages link tags
 *
 * @since Koutabase 1.0
 *
 * @param string $output Output from wp_list_pages.
 *
 * @return string Modified output.
 */
function koutabase_wp_list_pages_filter( $output ) {
	$output = preg_replace( '/<a(.*)href="([^"]*)"(.*)>(.*)<\/a>/', '<a$1 title="$4" href="$2"$3>$4</a>', $output );
	return $output;
}
add_filter( 'wp_list_pages', 'koutabase_wp_list_pages_filter' );

/**
 * Filter the excerpt length to 30 characters.
 *
 * @param int $length Excerpt length.
 *
 * @return int (Maybe) modified excerpt length.
 */
function koutabase_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'koutabase_excerpt_length' );


/************************************
4.0 - PAGE NAVI
 ************************************/

/**
 * Numeric Page Navi
 *
 * This is built into the theme by default. Call it using koutabase_page_navi().
 *
 * @since Koutabase 1.0
 *
 * @param string   $before Content to include before the nav.
 * @param string   $after Content to include after the nav.
 * @param WP_Query $query Current WordPress Query.
 */
function koutabase_page_navi( $before = '', $after = '', $query ) {
	$request        = $query->request;
	$posts_per_page = intval( get_query_var( 'posts_per_page' ) );
	$paged          = intval( get_query_var( 'paged' ) );
	$numposts       = $query->found_posts;
	$max_page       = $query->max_num_pages;

	if ( $numposts <= $posts_per_page || $max_page <= 1 ) {
		return;
	}

	if ( empty( $paged ) || 0 === $paged ) {
		$paged = 1;
	}

	$pages_to_show         = ( wp_is_mobile() ) ? 2 : 5;
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start       = floor( $pages_to_show_minus_1 / 2 );
	$half_page_end         = ceil( $pages_to_show_minus_1 / 2 );
	$start_page            = $paged - $half_page_start;

	if ( $start_page <= 0 ) {
		$start_page = 1;
	}

	$end_page = $paged + $half_page_end;

	if ( ( $end_page - $start_page ) !== $pages_to_show_minus_1 ) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}

	if ( $end_page > $max_page ) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page   = $max_page;
	}

	if ( $start_page <= 0 ) {
		$start_page = 1;
	}

	echo $before . '<nav class="page-navigation clearfix"><ol class="koutabase-page-navi clearfix">';

	if ( $start_page > 1 && $pages_to_show < $max_page ) {
		$first_page_text = __( 'Ensimmäinen', 'koutabase' );
		echo '<li class="kb-first-link"><a rel="prev" href="' . get_pagenum_link() . '" title="' . $first_page_text . '">' . $first_page_text . '</a></li>';
	}

	if ( $paged > 1 ) {
		echo '<li class="kb-prev-link">';
			previous_posts_link( '&laquo;' );
		echo '</li>';
	}

	for ( $i = $start_page; $i <= $end_page; $i++ ) {
		if ( $i == $paged ) {
			echo '<li class="kb-current"><span>' . esc_html( $i ) . '</span></li>';
		} elseif ( ( $paged - 1 ) === $i ) {
			echo '<li><a rel="prev" href="' . get_pagenum_link( $i ) . '" title="Näytä sivu ' . esc_html( $i ) . '">' . esc_html( $i ) . '</a></li>';
		} elseif ( ( $paged + 1 ) === $i ) {
			echo '<li><a rel="next" href="' . get_pagenum_link( $i ) . '" title="Näytä sivu ' . esc_html( $i ) . '">' . esc_html( $i ) . '</a></li>';
		} else {
			echo '<li><a href="' . get_pagenum_link( $i ) . '" title="Näytä sivu ' . esc_html( $i ) . '">' . esc_html( $i ) . '</a></li>';
		}
	}

	if ( $end_page < $max_page ) {
		echo '<li class="kb-next-link">';
			next_posts_link( '&raquo;' );
		echo '</li>';
	}

	if ( $end_page < $max_page ) {
		$last_page_text = __( 'Viimeinen', 'koutabase' );
		echo '<li class="kb-last-link"><a rel="next" href="' . get_pagenum_link( $max_page ) . '" title="' . $last_page_text . '">' . $last_page_text . '</a></li>';
	}

	echo '</ol></nav>' . $after . '';

} // end koutabase_page_navi()


/************************************
 * 5.0 - CUSTOM LOGIN
 *    5.1 - Add styles to login page
 *    5.2 - Change logo link
 *    5.3 - Change alt attribute on logo
 *************************************/

/**
 * Custom login page CSS
 *
 * @since Koutabase 1.0
 */
function koutabase_login_css() {
	echo '<link rel="stylesheet" href="' . esc_url( get_stylesheet_directory_uri() ) . '/css/login.css">';
}
add_action( 'login_head', 'koutabase_login_css' );

/**
 * Change logo link from wordpress.org to your site
 *
 * @since Koutabase 1.0
 */
function koutabase_login_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'koutabase_login_url' );

/**
 * Change alt text on logo to show your site name
 *
 * @since Koutabase 1.0
 */
function koutabase_login_title() {
	return get_option( 'blogname' );
}
add_filter( 'login_headertext', 'koutabase_login_title' );


/************************************
 * 6.0 - VISITOR/USER UX FUNCTIONS
 *    6.1 - Remove p tags from images
 *************************************/

/**
 * Remove the p from around imgs
 *
 * This function is called in koutabase_build().
 *
 * @link http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
 *
 * @since Koutabase 1.0
 *
 * @param string $content Content to be modified.
 *
 * @return string Modified content
 */
function koutabase_filter_ptags_on_images( $content ) {
	return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}

/************************************
 * 7.0 - TITLES
 *    7.1 - Get archive titles
 *************************************/

/**
 * Get the archive title
 *
 * @since Koutabase 1.0
 *
 * @return string title
 */
function koutabase_get_the_archive_title() {

	$title = get_the_archive_title();

	if ( is_tag() || is_category() || is_tax() ) {
		$title = single_term_title( '', false );
	} elseif ( is_home() ) {
		$title = get_the_title( get_option( 'page_for_posts' ) );
	} elseif ( function_exists( 'is_shop' ) ) {
		if ( is_shop() ) {
			$title = get_the_title( get_option( 'woocommerce_shop_page_id' ) );
		}
	} elseif ( is_search() ) {
		$title = esc_html__( 'Haku:', 'koutabase' ) . ' <span class="search-terms">' . get_search_query() . '</span>';
	} elseif ( is_404() ) {
		$title = '404';
	}
	return $title;

}
