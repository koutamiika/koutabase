<?php
/**
 * Widget related functionalities
 *
 * @package Koutabase
 */

/**
 * Unregister unnecessary widgets as they are not supported by the theme.
 */
function koutabase_unregister_widgets() {
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Search' );
    unregister_widget( 'WP_Widget_Categories' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    unregister_widget( 'WP_Widget_Media_Audio' );
    unregister_widget( 'WP_Widget_Media_Video' );
    unregister_widget( 'WP_Widget_Media_Gallery' );
}
add_action( 'widgets_init', 'koutabase_unregister_widgets' );
