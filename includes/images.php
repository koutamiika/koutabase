<?php
/**
 * Image and headers related functions.
 *
 * @package Koutabase
 */

// Set up the content width value based on the theme's design.
if ( ! isset( $content_width ) ) {
	$content_width = 1170;
}

/**
 * Adjust content_width value for image attachment template
 *
 * @since Koutabase 1.0
 */
function koutabase_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'koutabase_content_width' );

/**
 * Add additional image sizes
 *
 * Function called in koutabase_build() in base-functions.php.
 * Ex. add_image_size( 'koutabase-thumb-600', 600, 150, true );
 *
 * @since Koutabase 1.0
 */
function koutabase_add_image_sizes() {
	add_image_size( 'koutabase-thumb-550x310', 550, 310, true ); // 16:9 ratio...roughly.
}

/**
 * Set header image as a BG
 *
 * This is a callback function defined in koutabase_theme_support() 'custom-header'.
 *
 * @since Koutabase 1.0
 */
function koutabase_custom_headers_callback() {
	if ( has_header_image() ) {
		?>
<style type="text/css">#banner { display: block; background-image: url(<?php header_image(); ?>); }</style>
		<?php
	}
} // end koutabase_custom_headers_callback()
