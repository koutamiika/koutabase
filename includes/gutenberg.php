<?php
/**
 * Gutenberg related functionalities and custom block.
 *
 * @package Koutabase
 */

/**
 * ACF Custom Blocks.
 */
function koutabase_register_blocks() {
	if ( function_exists( 'acf_register_block_type' ) ) {

		/**
		 * Register custom blocks here
		 */

		/*
		acf_register_block_type(
			array(
				'name'            => 'custom-block',
				'title'           => __( 'Custom block name', 'koutabase' ),
				'render_template' => '',
				'mode'			  => '', // edit, preview, auto
				'enqueue_assets'  => function() {},
				'icon'            => '',
				'category'        => '',
				'align'           => '',
				'supports'        => array(),
			)
		);
		*/
	}
}
add_action( 'acf/init', 'koutabase_register_blocks' );

/**
 * Custom color palette
 */
function add_custom_gutenberg_color_palette() {
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => esc_html__( 'White', 'koutabase' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => esc_html__( 'Black', 'koutabase' ),
				'slug'  => 'black',
				'color' => '#000000',
			),
		)
	);
}
//add_action( 'after_setup_theme', 'add_custom_gutenberg_color_palette' );

function koutabase_acf_google_map_api( $api ) {
    $api['key'] = 'MYAPIKEY';
    return $api;
}
add_filter( 'acf/fields/google_map/api', 'koutabase_acf_google_map_api' );
