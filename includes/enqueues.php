<?php
/**
 * Enqueue all styles and scripts-
 *
 * @package Koutabase
 */

/**
 * Enqueue scripts and styles in wp_head() and wp_footer()
 *
 * This function is called in koutabase_build() in base-functions.php.
 *
 * @since Koutabase 1.0
 */
function koutabase_scripts_and_styles() {

	/**
	 * Add to wp_head()
	 */

	// Main stylesheet.
	wp_enqueue_style( 'koutabase-stylesheet', get_stylesheet_directory_uri() . '/css/style.css', array(), KOUTABASE_THEME_VERSION );

	// Modernizr - http://modernizr.com/.
	// update this to include only what you need to test.
	wp_enqueue_script( 'koutabase-modernizr', get_stylesheet_directory_uri() . '/libs/js/custom-modernizr.min.js', array(), '3.6.0', false );

	/**
	 * Add to wp_footer()
	 */

	// Magnific Popup (lightbox) - http://dimsemenov.com/plugins/magnific-popup/ .
	wp_enqueue_script( 'koutabase-magnific-popup-js', get_stylesheet_directory_uri() . '/libs/js/jquery.magnific-popup.min.js', array( 'jquery' ), '1.1.0', true );

	// Add Koutabase scripts file in the footer.
	wp_enqueue_script( 'koutabase-js', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ), KOUTABASE_THEME_VERSION, true );

} // end koutabase_scripts_and_styles()
