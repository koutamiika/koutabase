<?php
/**
 * No Banner Page Template
 *
 * Template name: Ei otsakekuvaa
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();
?>

<div id="inner-content">

	<?php
	/**
	 * Koutabase After Page #inner-content begin
	 */
	do_action( 'koutabase_after_page_inner_content_begin' );
	?>

	<div id="main" class="main clearfix" <?php koutabase_schema_markup( 'main', true ); ?>>

	<?php
	/**
	 * Koutabase After Page #main ends
	 */
	do_action( 'koutabase_after_page_main_begin' );
	?>

	<?php if ( have_posts() ) : ?>

		<?php while ( have_posts() ) : ?>

			<?php the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'page' ); ?>

		<?php endwhile; ?>

	<?php else : ?>

			<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

	<?php endif; ?>

	<?php
	/**
	 * Koutabase Before Page #main ends
	 */
	do_action( 'koutabase_before_page_main_end' );
	?>

	</div><?php // END #main. ?>

	<?php
	/**
	 * Koutabase Before Page #inner-content ends
	 */
	do_action( 'koutabase_before_page_inner_content_end' );
	?>

</div><?php // END #inner-content. ?>

<?php
get_footer();
