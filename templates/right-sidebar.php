<?php
/**
 * Right Sidebar Only Template
 *
 * Template name: Oikea sivupalkki
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Koutabase
 */

get_header();
?>

<?php get_template_part( 'template-parts/banner' ); // Banner ?>

<div id="inner-content" class="container">

	<div class="row <?php echo esc_attr( $kb_layout_class['row'] ); ?>">

		<div id="main" class="<?php echo esc_attr( $kb_layout_class['main'] ); ?> clearfix" role="main">

		<?php
		/**
		 * Koutabase After Page #main ends
		 */
		do_action( 'koutabase_after_page_main_begin' );
		?>

		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : ?>

				<?php the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'page' ); ?>

			<?php endwhile; ?>

		<?php else : ?>

				<?php get_template_part( 'template-parts/error' ); // WordPress template error message. ?>

		<?php endif; ?>

		<?php
		/**
		 * Koutabase Before Page #main ends
		 */
		do_action( 'koutabase_before_page_main_end' );
		?>

		</div><?php // END #main ?>

		<?php
		if ( is_active_sidebar( 'right-sidebar' ) ) :
			?>

			<div id="right-sidebar" class="sidebar <?php echo $kb_sidebar_class['right']; ?>" role="complementary">
				<?php dynamic_sidebar( 'right-sidebar' ); ?>
			</div>

			<?php
		endif;
		?>

	</div>

</div><?php // END #inner-content ?>

<?php
get_footer();
